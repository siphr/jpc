# Overview

Super simple stock market.

# General Instructions

    * make test: Run tests and print coverage.
    * make coverage: Run tests and generate an xml report.
    * make lint: Run flake8 linter for static analysis.
    * make build: Create a compressed package for delivery/deployment.
    * make clean: Build cleanup.





